package it.unibo.sa1415_selforg.main;

import alice.logictuple.LogicTuple;
import alice.logictuple.Var;
import alice.logictuple.exceptions.InvalidVarNameException;
import alice.tucson.api.AbstractSpawnActivity;
import alice.tucson.api.exceptions.TucsonInvalidLogicTupleException;

/**
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public final class AntSpawn extends AbstractSpawnActivity {

    /**
     * @param msg
     */
    private static void err(String msg) {
        System.err.println("[AntSpawn]: " + msg);
    }

    /*
     * (non-Javadoc)
     * @see alice.tucson.api.AbstractSpawnActivity#doActivity()
     */
    @Override
    public void doActivity() {
        LogicTuple me = null;
        try {
            me = rd(new LogicTuple("ant", new Var("ME")));
        } catch (InvalidVarNameException | TucsonInvalidLogicTupleException e) {
            AntSpawn.err("Exception " + e.toString() + ", caused by "
                    + e.getCause().toString());
        }
        log("Hello, I'm " + me.getArg(0) + " on " + getTargetTC());
        try {
            log("Sampling item...");
            LogicTuple item = urd(new LogicTuple("item", new Var("COLOR")));
            log(item.toString() + " sampled");
            log("Sampling neighbour...");
            LogicTuple nbr = urd(new LogicTuple("nbr", new Var("NBR")));
            log(nbr.toString() + " sampled");

        } catch (InvalidVarNameException | TucsonInvalidLogicTupleException e) {
            AntSpawn.err("Exception " + e.toString() + ", caused by "
                    + e.getCause().toString());
        }
    }

}
