package it.unibo.sa1415_selforg.main;

import java.util.LinkedList;
import java.util.List;
import alice.logictuple.LogicTuple;
import alice.logictuple.Value;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.EnhancedACC;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonMetaACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;

/**
 * Entry point. After launching a TuCSoN node on your host machine, launch this
 * giving as parameters: (i) your ant name (starting lowercase, no special
 * chars); (ii) IP address of the local host machine where you started your
 * TuCSoN Node on; (iii) TCP port number where you started your TuCSoN Node on;
 * (iv) the list of neighbourhood nodes you are logically connected to (full
 * tuple centre name, e.g. default@127.0.0.1:20504)
 * 
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public final class Launcher {

    private static String antname;
    private static TucsonTupleCentreId localnode;
    private static List<TucsonTupleCentreId> neighbours;
    private static EnhancedACC acc;

    /**
     * @param args
     *            (i) your ant name (starting lowercase, no special chars); (ii)
     *            IP address of the local host machine where you started your
     *            TuCSoN Node on; (iii) TCP port number where you started your
     *            TuCSoN Node on; (iv) the list of neighbourhood nodes you are
     *            logically connected to (full tuple centre name, e.g.
     *            default@127.0.0.1:20504)
     */
    public static void main(String[] args) {
        parseArgs(args);
        configNode();
        acquireACC();
        spawnAnt();
        Launcher.log("Done, now let the system self-organise!");
    }

    /**
     * Spawns an ant named {@code $antname} into local TuCSoN node
     */
    private static void spawnAnt() {
        Launcher.log("Spawning ant " + antname + " into " + localnode);
        try {
            acc.spawn(
                    localnode,
                    LogicTuple
                            .parse("exec('it.unibo.sa1415_selforg.main.AntSpawn.class')"),
                    Long.MAX_VALUE);
        } catch (InvalidLogicTupleException
                | TucsonOperationNotPossibleException
                | UnreachableNodeException | OperationTimeOutException e) {
            Launcher.err("Exception " + e.toString() + ", caused by "
                    + e.getCause().toString());
        }
    }

    /**
     * Acquires a TuCSoN ACC
     */
    private static void acquireACC() {
        acc = null;
        try {
            acc = TucsonMetaACC.getContext(
                    new TucsonAgentId(antname.concat("L")),
                    localnode.getNode(), localnode.getPort());
        } catch (TucsonInvalidAgentIdException e) {
            Launcher.err("Invalid TuCSoN agent ID");
            System.exit(-1);
        }
    }

    /**
     * Puts the ant name ({@code ant($antname)}) and the list of neighbours (
     * {@code nbrs($neighbourtcs)}) into the local TuCSoN node
     */
    private static void configNode() {
        LogicTuple tuple = new LogicTuple("ant", new Value(antname));
        Launcher.log("Putting tuple " + tuple + " into " + localnode);
        try {
            acc.out(localnode, tuple, Long.MAX_VALUE);
            tuple = asListTuple(neighbours);
            Launcher.log("Putting tuple " + tuple + " into " + localnode);
            acc.outAll(localnode, tuple, Long.MAX_VALUE);
        } catch (TucsonOperationNotPossibleException | UnreachableNodeException
                | OperationTimeOutException e) {
            Launcher.err("Exception " + e.toString() + ", caused by "
                    + e.getCause().toString());
        }
        /*
         * TODO put the tuples to cluster
         */
    }

    /**
     * @param nbrs
     *            the list of neighbours' TuCSoN tuple centre IDs
     * @return the String representation of the Prolog list of neighbours'
     *         TuCSoN tuple centre IDs
     */
    private static LogicTuple asListTuple(List<TucsonTupleCentreId> nbrs) {
        StringBuffer nbrsString = new StringBuffer("[");
        for (TucsonTupleCentreId tcid : nbrs) {
            nbrsString.append("nbr(").append(tcid.toString()).append("),");
        }
        LogicTuple listTuple = null;
        try {
            listTuple = LogicTuple.parse((nbrsString.replace(
                    nbrsString.length() - 1, nbrsString.length(), "]")
                    .toString()));
        } catch (InvalidLogicTupleException e) {
            Launcher.err("Exception " + e.toString() + ", caused by "
                    + e.getCause().toString());
        }
        return listTuple;
    }

    /**
     * @param args
     *            see {@link Launcher#main()}
     */
    private static void parseArgs(String[] args) {
        if (args.length < 4) {
            Launcher.err("No args given, at least 4 expected: $1=antname $2=netid $3=portno $4+=[neighbourtcs]");
            System.exit(-1);
        }
        antname = args[0].toLowerCase();
        final String netid = args[1];
        final String portno = args[2];
        Launcher.log("Welcome " + antname + " on node " + netid + ":" + portno
                + ", enjoy your stay!");
        neighbours = new LinkedList<>();
        int i = 3;
        try {
            for (; i < args.length; i++) {
                neighbours.add(new TucsonTupleCentreId(args[i]));
                Launcher.log("Neighbour " + neighbours.get(i - 3)
                        + " recognised");
            }
        } catch (final TucsonInvalidTupleCentreIdException e) {
            Launcher.err("Neighbour " + i
                    + " is not a valid TuCSoN tuple centre ID");
            System.exit(-1);
        }
        try {
            localnode = new TucsonTupleCentreId("default", netid, portno);
        } catch (TucsonInvalidTupleCentreIdException e) {
            Launcher.err("Given TCP/IP address " + netid + ":" + portno
                    + " is not a valid TCP/IP address");
            System.exit(-1);
        }
    }

    /**
     * @param msg
     *            the msg to log on std err
     */
    private static void log(String msg) {
        System.out.println("[Launcher]: " + msg);
    }

    /**
     * @param msg
     *            the msg to log on std err
     */
    private static void err(String msg) {
        System.err.println("[Launcher]: " + msg);
    }

}
