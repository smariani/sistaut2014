package env;

import jason.asSyntax.Literal;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.Structure;
import jason.environment.Environment;
import jason.environment.grid.Location;

import java.util.logging.Logger;

/**
 * Any Jason environment "entry point" should extend jason.environment.Environment class
 * to override methods init(), updatePercepts() and executeAction().
 */
public class HouseEnv extends Environment {

	// action literals
	public static final Literal of = Literal.parseLiteral("open(fridge)");
	public static final Literal clf = Literal.parseLiteral("close(fridge)");
	public static final Literal gb = Literal.parseLiteral("get(beer)");
	public static final Literal hb = Literal.parseLiteral("hand_in(beer)");
	public static final Literal sb = Literal.parseLiteral("sip(beer)");

	// belief literals
	public static final Literal hob = Literal.parseLiteral("has(owner,beer)");
	public static final Literal af = Literal.parseLiteral("at(robot,fridge)");
	public static final Literal ao = Literal.parseLiteral("at(robot,owner)");

	static Logger logger = Logger.getLogger(HouseEnv.class.getName());

	HouseModel model; // the model of the grid

	@Override
	public void init(String[] args) {
		model = new HouseModel();

		if (args.length == 1 && args[0].equals("gui")) {
			HouseView view = new HouseView(model);
			model.setView(view);
		}
		// boot the agents' percepts
		updatePercepts();
	}

	/**
	 * Update the agents' percepts based on current state of the environment
	 * (HouseModel)
	 */
	void updatePercepts() {
		// clear the percepts of the agents
		clearPercepts("robot");
		clearPercepts("owner");

		// get the robot location
		Location lRobot = model.getAgPos(0);

		// the robot can perceive where it is
		if (lRobot.equals(model.lFridge)) {
			addPercept("robot", af);
		}
		if (lRobot.equals(model.lOwner)) {
			addPercept("robot", ao);
		}

		// the robot can perceive the beer stock only when at the (open) fridge
		if (model.fridgeOpen) {
			addPercept(
					"robot",
					Literal.parseLiteral("stock(beer," + model.availableBeers
							+ ")"));
		}

		// the robot can perceive if the owner has beer (the owner too)
		if (model.sipCount > 0) {
			addPercept("robot", hob);
			addPercept("owner", hob);
		}
	}

	/**
	 * The <code>boolean</code> returned represents the action "feedback"
	 * (success/failure)
	 */
	@Override
	public boolean executeAction(String ag, Structure action) {
		System.out.println("[" + ag + "] doing: " + action);
		boolean result = false;
		if (action.equals(of)) { // of = open(fridge)
			result = model.openFridge();
		} else if (action.equals(clf)) { // clf = close(fridge)
			result = model.closeFridge();
		} else if (action.getFunctor().equals("move_towards")) {
			String l = action.getTerm(0).toString(); // get where to move
			Location dest = null;
			if (l.equals("fridge")) {
				dest = model.lFridge;
			} else if (l.equals("owner")) {
				dest = model.lOwner;
			}
			result = model.moveTowards(dest);
		} else if (action.equals(gb)) { // gb = get(beer)
			result = model.getBeer();
		} else if (action.equals(hb)) { // hb = hand_in(beer)
			result = model.handInBeer();
		} else if (action.equals(sb)) { // sb = sip(beer)
			result = model.sipBeer();
		} else if (action.getFunctor().equals("deliver")) {
			// simulate delivery time
			try {
				Thread.sleep(4000);
				result = model.addBeer((int) ((NumberTerm) action.getTerm(1))
						.solve()); // add the number of beers delivered
			} catch (Exception e) {
				logger.info("Failed to execute action deliver!" + e);
			}

		} else {
			logger.info("Failed to execute action " + action);
		}
		// only if action completed successfully, update agents' percepts
		if (result) {
			updatePercepts();
			try {
				Thread.sleep(100);
			} catch (Exception e) {
			}
		}
		return result;
	}
}
