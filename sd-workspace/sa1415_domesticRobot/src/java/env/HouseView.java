package env;

import jason.environment.grid.GridWorldView;
import jason.environment.grid.Location;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

/**
 * Jason provides a convenient GridWorldView class representing
 * the view of a square environment consisting of a grid of tiles.
 * Less conveniently, the Javadoc is almost useless thus you should
 * figure out by yourself (e.g. by looking at comments in examples
 * source code) how the things work.
 */
public class HouseView extends GridWorldView {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	HouseModel hmodel;

	public HouseView(HouseModel model) {
		super(model, "Domestic Robot", 700);
		hmodel = model;
		defaultFont = new Font("Arial", Font.BOLD, 16); // change default font
		setVisible(true);
		repaint();
	}

	/** draw application objects */
	@Override
	public void draw(Graphics g, int x, int y, int object) {
		Location lRobot = hmodel.getAgPos(0);
		super.drawAgent(g, x, y, Color.lightGray, -1);
		switch (object) {
		case HouseModel.FRIDGE:
			if (lRobot.equals(hmodel.lFridge)) {
				super.drawAgent(g, x, y, Color.yellow, -1);
			}
			g.setColor(Color.black);
			drawString(g, x, y, defaultFont, "Fridge (" + hmodel.availableBeers
					+ ")");
			break;
		case HouseModel.OWNER:
			if (lRobot.equals(hmodel.lOwner)) {
				super.drawAgent(g, x, y, Color.yellow, -1);
			}
			String o = "Owner";
			if (hmodel.sipCount > 0) {
				o += " (" + hmodel.sipCount + ")";
			}
			g.setColor(Color.black);
			drawString(g, x, y, defaultFont, o);
			break;
		}
	}

	@Override
	public void drawAgent(Graphics g, int x, int y, Color c, int id) {
		Location lRobot = hmodel.getAgPos(0);
		if (!lRobot.equals(hmodel.lOwner) && !lRobot.equals(hmodel.lFridge)) {
			c = Color.yellow;
			if (hmodel.carryingBeer)
				c = Color.orange;
			super.drawAgent(g, x, y, c, -1);
			g.setColor(Color.black);
			super.drawString(g, x, y, defaultFont, "Robot");
		}
	}
}
