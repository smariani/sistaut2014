package env;

import jason.environment.grid.GridWorldModel;
import jason.environment.grid.Location;

/**
 * Jason provides a convenient GridWorldModel class representing
 * the model of a square environment consisting of a grid of tiles.
 * Less conveniently, the Javadoc is almost useless thus you should
 * figure out by yourself (e.g. by looking at comments in examples
 * source code) how the things work.
 */
public class HouseModel extends GridWorldModel {

	// constants used by the view component (HouseView) to draw environment "objects"
	public static final int FRIDGE = 16;
	public static final int OWNER = 32;
	// the grid size
	public static final int GSize = 7;
	// whether the fridge is open
	boolean fridgeOpen = false;
	// whether the robot is carrying beer
	boolean carryingBeer = false;
	// how many sips the owner did
	int sipCount = 0;
	// how many beers are available
	int availableBeers = 2;

	// where the environment objects are
	Location lFridge = new Location(0, 0);
	Location lOwner = new Location(GSize - 1, GSize - 1);

	public HouseModel() {
		// create a 7x7 grid with one mobile agent
		super(GSize, GSize, 1);
		/*
		 * initial location of robot (column 3, line 3)
		 * ag code 0 means the robot
		 */
		setAgPos(0, GSize / 2, GSize / 2);
		// initial location of fridge and owner
		add(FRIDGE, lFridge);
		add(OWNER, lOwner);
	}
	
	/*
	 * All the following methods are invoked by the environment controller (HouseEnv)
	 * so as to model changes in the environment, either spontaneous or due to agents
	 * interaction. As such, they first check actions pre-conditions, then carry out
	 * actions post-conditions.
	 */

	boolean openFridge() {
		if (!fridgeOpen) {
			fridgeOpen = true;
			return true;
		} else {
			return false;
		}
	}

	boolean closeFridge() {
		if (fridgeOpen) {
			fridgeOpen = false;
			return true;
		} else {
			return false;
		}
	}

	boolean moveTowards(Location dest) {
		Location r1 = getAgPos(0);
		// compute where to move
		if (r1.x < dest.x)
			r1.x++;
		else if (r1.x > dest.x)
			r1.x--;
		if (r1.y < dest.y)
			r1.y++;
		else if (r1.y > dest.y)
			r1.y--;
		setAgPos(0, r1); // actually move the robot in the grid
		// repaint fridge and owner locations (to repaint colors)
		if (view != null) {
			view.update(lFridge.x, lFridge.y);
			view.update(lOwner.x, lOwner.y);
		}
		return true;
	}

	boolean getBeer() {
		if (fridgeOpen && availableBeers > 0 && !carryingBeer) {
			availableBeers--;
			carryingBeer = true;
			if (view != null)
				view.update(lFridge.x, lFridge.y);
			return true;
		} else {
			return false;
		}
	}

	boolean addBeer(int n) {
		availableBeers += n;
		if (view != null)
			view.update(lFridge.x, lFridge.y);
		return true;
	}

	boolean handInBeer() {
		if (carryingBeer) {
			sipCount = 10;
			carryingBeer = false;
			if (view != null)
				view.update(lOwner.x, lOwner.y);
			return true;
		} else {
			return false;
		}
	}

	boolean sipBeer() {
		if (sipCount > 0) {
			sipCount--;
			if (view != null)
				view.update(lOwner.x, lOwner.y);
			return true;
		} else {
			return false;
		}
	}
}
